# Features

Add Go Snippets to VS Code

## Channels

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>chus</strong></font> | declare a *<-chan* unbuffered. |
| <font color="#c6b925"><strong>chur</strong></font> | declare a *chan<-* unbuffered. |
| <font color="#c6b925"><strong>chbs</strong></font> | declare a *<-chan* buffered. |
| <font color="#c6b925"><strong>chur</strong></font> | declare a *chan<-* buffered. |

---

### Conditions

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>ifok</strong></font> | if ok idiom statement. |
| <font color="#c6b925"><strong>ifelse</strong></font> | if else statement. |
| <font color="#c6b925"><strong>sw, switch</strong></font> | switch statement. |
| <font color="#c6b925"><strong>sdef,def</strong></font> | switch default case. |

---

### fmt

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>fpf</strong></font> | fmt.Fprintf(). |
| <font color="#c6b925"><strong>fpl</strong></font> | fmt.Fprintln(). |
| <font color="#c6b925"><strong>pf</strong></font> | fmt.Printf(). |
| <font color="#c6b925"><strong>pl</strong></font> | fmt.Println(). |
| <font color="#c6b925"><strong>spf</strong></font> | fmt.Sprintf(). |
| <font color="#c6b925"><strong>spl</strong></font> | fmt.Sprintln(). |

---

### Functions

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>funcinit, fninit, finit</strong></font> | declare init function. |
| <font color="#c6b925"><strong>funcm, fnm, fmain</strong></font> | declare a main function. |
| <font color="#c6b925"><strong>func, fn</strong></font> | declare a function. |
| <font color="#c6b925"><strong>funcr, fnr</strong></font> | declare a receiver function. |
| <font color="#c6b925"><strong>funcr*, fnr*</strong></font> | declare a receiver pointer function. |
| <font color="#c6b925"><strong>funct, fnt</strong></font> | declare a TestXXX function. |
| <font color="#c6b925"><strong>funct_, fnt_</strong></font> | declare a Test_XXX function. |
| <font color="#c6b925"><strong>funcfuzz, fnfuzz, fuzz</strong></font> | declare a Fuzz test function. |

---

### Go routines

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>gofunc, gofn</strong></font> | declare a goroutine anonymous function. |

---

### Imports

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>import, imp</strong></font> | import multiple packages. |

---

### Interfaces

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>itf</strong></font> | interface declaration. |

---

### Loops

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>fori</strong></font>  | for with index. |
| <font color="#c6b925"><strong>forr</strong></font>  | for range with key,value :=... |
| <font color="#c6b925"><strong>forrc</strong></font>  | for range channel. |
| <font color="#c6b925"><strong>forrv</strong></font> | for range with _,value :=... |

---

### Maps

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>delm</strong></font>  | delete a map element. |
| <font color="#c6b925"><strong>maps, smap</strong></font>  | map short declaration. |
| <font color="#c6b925"><strong>mampn, nmap</strong></font> | map nil initialization. |
| <font color="#c6b925"><strong>mapm, mmap</strong></font>  | make map initialization. |

---

### Packages

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>pkg</strong></font> | declare package with parent directory name. |

---

### Other

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>cl</strong></font> | close. |

---

### Return values

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>rn</strong></font>  | return nil. |
| <font color="#c6b925"><strong>rne</strong></font> | return nil, error. |
| <font color="#c6b925"><strong>re</strong></font>  | return error. |

---

### Slices

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>rn</strong></font>  | return nil. |
| <font color="#c6b925"><strong>rne</strong></font> | return nil, error. |
| <font color="#c6b925"><strong>re</strong></font>  | return error. |

---

### Templates

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>goapp</strong></font>  | declare package and main function replace all file content. |

---

### Types and Vars

| prefix | description |
| ------ | ----------- |
| <font color="#c6b925"><strong>iota</strong></font>   | declare iota constant. |
| <font color="#c6b925"><strong>struct, st</strong></font> | declare a struct type. |
| <font color="#c6b925"><strong>v:</strong></font>     | short var declaration. |
| <font color="#c6b925"><strong>vars</strong></font>   | declare multiple variables. |
  
---
---

## Requirements

Run only with *.go files.

## Extension Settings

Include if your extension adds any VS Code settings through the contributes.configuration extension point.
For example:
This extension contributes the following settings:

* myExtension.enable: Enable/disable this extension.
* myExtension.thing: Set to blah to do something.

### 1.0.0

First release

---
